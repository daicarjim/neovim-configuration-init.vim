"autocompletion javascript
filetype plugin on
set omnifunc=syntaxcomplete#complete
set title  " Muestra el nombre del archivo en la ventana de la terminal
set number  " Muestra los números de las líneas
"set mouse=a  " Permite la integración del mouse (seleccionar texto, mover el cursor)
set numberwidth=1
"set clipboard=unnamed
syntax enable
set showcmd
set ruler
set encoding=utf-8
set showmatch
set relativenumber
set laststatus=2
set nowrap  " No dividir la línea si es muy larga

set cursorline  " Resalta la línea actual
set colorcolumn=180  " Muestra la columna límite a 180 caracteres

" Indentación a 2 espacios
set tabstop=2
set shiftwidth=2
set softtabstop=2
set shiftround
set expandtab  " Insertar espacios en lugar de <Tab>s

set pastetoggle=<F3>

set hidden  " Permitir cambiar de buffers sin tener que guardarlos

set ignorecase  " Ignorar mayúsculas al hacer una búsqueda
set smartcase  " No ignorar mayúsculas si la palabra a buscar contiene mayúsculas

set spelllang=en,es  " Corregir palabras usando diccionarios en inglés y español

set termguicolors  " Activa true colors en la terminal
set background=dark  " Fondo del tema: light o dark
"colorscheme ron  " Nombre del tema

" Avoid backups and extra files creation

set nowritebackup
set noswapfile
set nobackup

"Division de pantalla al lado derecho

"set splitright



call plug#begin('~/.vim/plugged')

"themes
Plug 'morhetz/gruvbox'

"jump text

Plug 'easymotion/vim-easymotion'

"tree

Plug 'scrooloose/nerdtree'

"jump files

Plug 'christoomey/vim-tmux-navigator'

"autocompletion javascript

Plug 'ternjs/tern_for_vim', { 'do' : 'npm install' }

" Javascript support

Plug 'pangloss/vim-javascript'

" Typescript syntax

Plug 'leafgarland/typescript-vim' 

" Js an JSX syntax for React

Plug 'maxmellon/vim-jsx-pretty'   

" GraphQL syntax

Plug 'jparise/vim-graphql'     

" Completion code CoC

Plug 'neoclide/coc.nvim' , { 'branch' : 'release' }

" Html emmet
  
Plug 'mattn/emmet-vim'
 
" Tabnine autocompletion IA
 
Plug 'codota/tabnine-vim'

" Eslint

Plug 'w0rp/ale' 

" Styled Components

Plug 'styled-components/vim-styled-components', { 'branch': 'main' }

" Prettier

Plug 'sbdchd/neoformat'



call plug#end()

colorscheme gruvbox
let g:gruvbox_contrast_dark = "hard"
let NERDTreeQuitOnOpen=1
let NERDTreeShowHidden=1
let mapleader=" "
let g:coc_global_extensions = [ 'coc-tsserver' ]
let g:user_emmet_leader_key=','
let g:ale_sign_error = 'X' 
let g:ale_sign_warning = '!'
highlight ALEErrorSign ctermbg=NONE ctermfg=red
highlight ALEWarningSign ctermbg=NONE ctermfg=yellow

nmap <Leader>s <Plug>(easymotion-s2)
nmap <Leader>t :NERDTreeFind<CR>
" Remap keys for applying codeAction to the current line.
nmap <leader>c  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>f  <Plug>(coc-fix-current)
" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)


nmap <Leader>s :bel:term<CR>

autocmd BufWritePre,TextChanged,InsertLeave *.js Neoformat

